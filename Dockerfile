FROM servicerocket/saddlebred-jenkinsslave
MAINTAINER ServiceRocket Tools

ENV NODE_VERSION 0.12
ENV NPM_VERSION 2.5.0

ENV NVM_DIR /nvm
ENV PROFILE /etc/bash.bashrc
RUN mkdir $NVM_DIR \
  && chmod a+rw $NVM_DIR \
  && mkdir p /npm \
  && chmod a+rw /npm

RUN curl https://raw.githubusercontent.com/creationix/nvm/v0.23.3/install.sh | sh \
  && [ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh" \
  && nvm install $NODE_VERSION \
  && nvm use $NODE_VERSION \
  && nvm alias default $NODE_VERSION \
  && npm install -g npm@"$NPM_VERSION" \
  && npm install -g bower \
  && cd /npm \
  && npm install --save lineman \
  && cd /npm/node_modules/lineman \
  && npm link


RUN gpg --keyserver hkp://keys.gnupg.net --recv-keys D39DC0E3 && \
    curl -L https://get.rvm.io | bash -s stable
ENV PATH /usr/local/rvm/rubies/ruby-2.1.3/bin:/usr/local/rvm/bin:$PATH
RUN rvm-shell && \
    rvm requirements && \
    rvm install 2.1.3 && \
    rvm use 2.1.3 --default

# Global install required Ruby modules for activerecord
RUN gem install sass

# Standard SSH port
EXPOSE 22

CMD ["/usr/sbin/sshd", "-D"]

